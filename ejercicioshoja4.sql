﻿USE ciclistas;

-- 1 ejercicio

  SELECT  DISTINCT c.nombre, c.edad  FROM etapa e   JOIN ciclista c USING (dorsal);

-- 2 ejercicio

  SELECT  DISTINCT c.nombre, c.edad  FROM puerto p JOIN ciclista c USING (dorsal);

-- 3 ejercicio

  SELECT  DISTINCT c.nombre, c.edad  FROM (etapa e  JOIN ciclista c USING (dorsal)) JOIN puerto p USING (dorsal);

-- 4 ejercicio

  SELECT  DISTINCT e1.director  FROM (etapa e   JOIN ciclista c USING (dorsal))   JOIN equipo e1 USING (nomequipo); 

-- 5 ejercicio

  SELECT DISTINCT c.dorsal, c.nombre  FROM ciclista c   JOIN lleva l USING (dorsal);

-- 6 ejercicio

  SELECT 
    DISTINCT c.dorsal, c.nombre  FROM ciclista c  JOIN lleva l USING (dorsal)  WHERE l.código='MGE';

-- 7 ejercicio

  SELECT  DISTINCT c.dorsal  FROM (ciclista c  JOIN lleva l USING (dorsal))  JOIN etapa e USING (dorsal);

-- 8 ejercicio

  SELECT  DISTINCT e.numetapa  FROM etapa e JOIN puerto p USING (numetapa);

-- 9 ejercicio

  SELECT DISTINCT e.kms   FROM (etapa e   JOIN ciclista c USING (dorsal)) JOIN puerto p USING (numetapa)  WHERE c.nomequipo='Banesto';

-- 10 ejercicio

  SELECT   COUNT(DISTINCT e.dorsal) ciclistas_ganado FROM etapa e  JOIN puerto p USING (numetapa);  

--  11 ejercicio

  SELECT p.nompuerto  FROM puerto p JOIN ciclista c USING (dorsal)  WHERE c.nomequipo='Banesto';

-- 12 ejercicio

  SELECT  COUNT(DISTINCT e.numetapa)etapas  FROM (etapa e JOIN ciclista c USING (dorsal))  
    JOIN puerto p USING (numetapa)    WHERE c.nomequipo='Banesto'  AND e.kms>200;
